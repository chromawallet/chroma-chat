import * as React from 'react';

import { Modal, Input, Button } from 'semantic-ui-react';
import {makeKeyPair} from "../utils/Crypto";
import {Member} from "../types";
import {inviteUser} from '../bc-logic/blockchain';


export interface InviteUserProps {
    member: Member,
}

export interface InviteUserState {
    modalOpen: boolean;
    newUserPubKey: string;
    amount: number;
}

export class InviteUser extends React.Component<InviteUserProps, InviteUserState> {

    constructor(props: InviteUserProps) {
        super(props);
        this.state = {
            modalOpen: false,
            newUserPubKey: "",
            amount: 0,
        }
    }

    inviteUser = async () => {
        try {
            await inviteUser(this.props.member, this.state.newUserPubKey, this.state.amount);
            this.handleClose();
        } catch(e) {
            console.error(e);
        }

    };

    handleOpen = () => this.setState({ modalOpen: true });
    handleClose = () => this.setState({ modalOpen: false });

    render () {
        const kp = makeKeyPair();
        console.log(kp.pubKey.toString('hex'), kp.privKey.toString('hex'));

        return <Modal open={this.state.modalOpen} onClose={this.handleClose} trigger={<Button onClick={this.handleOpen}>Invite user</Button>}>
    <Modal.Header>Set up your identity</Modal.Header>
        <Modal.Content>
            <h5>Public key of the new user: </h5>
            <Input fluid onChange={(e, data) => {e.preventDefault(); this.setState({newUserPubKey: data.value})}}/>
            <h5>Amount to transfer:</h5>
            <Input fluid type={'integer'} onChange={(e, data) => {e.preventDefault(); this.setState({amount: data.value as unknown as number})}}/>
        </Modal.Content>
        <Modal.Actions>
        <Button
        primary
        icon={"add"}
        labelPosition={'right'}
        content={"Create user"}
        onClick={this.inviteUser}
        />
    </Modal.Actions>
    </Modal>
    }
}