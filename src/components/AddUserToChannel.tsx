import * as React from 'react';

import {Button, Input, Modal} from 'semantic-ui-react';

import {Member} from "../types";
import {SyntheticEvent} from "react";
import {inviteUserToChat} from '../bc-logic/blockchain';


export interface AddUserToChannelProps {
    member: Member;
    channel: string;
}

export interface AddUserToChannelState {
    modalOpen: boolean;
    invitedPubKey: string;
}

export class AddUserToChannel extends React.Component<AddUserToChannelProps, AddUserToChannelState> {

    constructor(props: AddUserToChannelProps) {
        super(props);

        this.state = {
            modalOpen: false,
            invitedPubKey: "",
        }
    }

    addUserToChannel = async (e: SyntheticEvent<HTMLElement>) => {
        e.preventDefault();
        try {
            await inviteUserToChat(this.props.member, this.props.channel, this.state.invitedPubKey);
            this.handleClose()
        }catch(e) {
            console.error(e);
        }
    };

    handleOpen = () => this.setState({modalOpen: true});
    handleClose = () => this.setState({modalOpen: false});

    render() {
        return <Modal open={this.state.modalOpen} onClose={this.handleClose} trigger={
                                        <Button
                                            style={{marginTop: '1em', float: 'right'}}
                                            onClick={this.handleOpen
                                        }>Add User</Button>
                }>
                    <Modal.Header>Set up your identity</Modal.Header>
                    <Modal.Content>
                        <h5>Public key of the user: </h5>
                        <Input fluid onChange={(e, data) => {
                            e.preventDefault();
                            this.setState({invitedPubKey: data.value})
                        }}/>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button
                            primary
                            icon={"add"}
                            labelPosition={'right'}
                            content={"Invite User"}
                            onClick={this.addUserToChannel}
                        />
                    </Modal.Actions>
                </Modal>
    }
}