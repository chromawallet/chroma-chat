import * as React from "react";
import {SyntheticEvent} from "react";

import * as DOMPurify from 'dompurify'

import {
    Button,
    Grid,
    Header,
    // Image,
    Input,
    InputOnChangeData,
    Modal
} from 'semantic-ui-react';
import {makeKeyPair, toBuffer, verifyKeyPair} from "../utils/Crypto";
import {generateImg} from "../utils/avatar";



export interface LoginProps {
    login: Function,
}

export interface LoginState {
    privKey: string,
    pubKey: string,
    image: string
}

export class Login extends React.Component<LoginProps, LoginState> {

    constructor(props: LoginProps) {
        super(props);

        this.state = {
            privKey: "",
            pubKey: "",
            image: "",
        };

        this.login = this.login.bind(this);
        this.handlePrivKeyChange = this.handlePrivKeyChange.bind(this);
        this.generateKeyPair = this.generateKeyPair.bind(this);
        this.isError = this.isError.bind(this);
    }



    login(e: SyntheticEvent<HTMLElement>) {
        e.preventDefault();
        this.props.login({pubKey: this.state.pubKey.toUpperCase(), privKey: this.state.privKey.toUpperCase()})
    }

    handlePrivKeyChange(e: SyntheticEvent<HTMLElement>, data: InputOnChangeData) {
        const privKey: string = data.value;
        this.setState({privKey});

        try{
            const member = verifyKeyPair(toBuffer(privKey));
            this.setState({
                pubKey: member.pubKey.toString('hex'),
                image: generateImg(member.pubKey.toString('hex'))
            });
        } catch (e) {
            this.setState({pubKey: ""})
            console.error("Key is not valid");
        }
    }

    generateKeyPair() {
        const kp = makeKeyPair();
        this.setState({
            pubKey: kp.pubKey.toString("hex"),
            privKey: kp.privKey.toString("hex"),
            image: generateImg(kp.pubKey.toString('hex'))
        })
    }

    isError(): boolean {
        try {
            verifyKeyPair(toBuffer(this.state.privKey));
            return false;
        } catch(e) {
            return this.state.privKey.length != 0;
        }
    }

    render() {
        return <Modal open>
            <Modal.Header>Set up your identity</Modal.Header>
            <Modal.Content>
                {/*<Image wrapped size='medium' src={this.state.image} />*/}
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={6}>
                            <div style={{width: "200px"}} dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(this.state.image)}}></div>
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Header>Specify your keys</Header>
                            <h5>Public key</h5> <Input fluid disabled value={this.state.pubKey} /><br/>
                            <h5>Private Key</h5> <Input fluid error={this.isError()} value={this.state.privKey} onChange={this.handlePrivKeyChange} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    icon={"shuffle"}
                    labelPosition={'right'}
                    content={"Generate keypair"}
                    onClick={this.generateKeyPair}
                />
                {this.state.privKey && this.state.pubKey && <Button
                    primary
                    icon='checkmark'
                    labelPosition='right'
                    content="Get in"
                    onClick={this.login}
                />}
            </Modal.Actions>
        </Modal>
    }
}