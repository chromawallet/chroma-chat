import * as React from 'react';
import TextareaAutosize from 'react-textarea-autosize';

import { Button, Form, Icon } from 'semantic-ui-react';
import {ChangeEvent, SyntheticEvent} from "react";
import {Member} from "../types";

import {postMessage} from "../bc-logic/blockchain";

interface NewMessageProps {
    memeber: Member;
    channel: string;
    onNewMessage: Function;
}
interface NewMessageState {
    newMessage: string;
}

export class NewMessage extends React.Component<NewMessageProps, NewMessageState> {

    constructor(props: NewMessageProps) {
        super(props);

        this.state = {
            newMessage: '',
        }
    }

    onNewMessage = async (e: SyntheticEvent<HTMLElement>) => {
        e.preventDefault();

        if(this.state.newMessage) {
            // Try to send message to bc
            try{
                await postMessage(this.props.memeber, this.props.channel, this.state.newMessage);
                this.setState({newMessage: ""});
                this.props.onNewMessage();
            } catch(e) {
                console.error(e);
            }
        } else {
            console.error("Text field is empty")
        }
    };

    onMessageChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
        e.preventDefault();
        this.setState({newMessage: e.target.value});
    };

    onKeyPress = (e: any) => {
        if(e.key === 'Enter'){
            this.onNewMessage(e);
        }
    };

    render() {
        return <Form style={{display: 'flex'}}>
            <TextareaAutosize
                style={{
                    minHeight: '3em',
                    padding: 5
                }}
                onChange={this.onMessageChange}
                minRows={1}
                maxRows={6}
                value={this.state.newMessage}
                onKeyPress={this.onKeyPress}
                placeholder="Write a message..."
            />
            <Button
                onClick={this.onNewMessage}
                disabled={!this.state.newMessage}
                icon
                >
                <Icon name='send' />
            </Button>
        </Form>
    }
}