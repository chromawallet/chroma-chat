import * as React from 'react';
import { Button, Input, Modal } from "semantic-ui-react";
import {Member} from 'src/types';
import {createChannel} from '../bc-logic/blockchain';
import {SyntheticEvent} from "react";

export interface NewChannelProps {
    member: Member;
    onNewChannelCreated: Function;
}

export interface NewChannelState {
    channelName: string;
    modalOpen: boolean;
}


export class NewChannel extends React.Component<NewChannelProps, NewChannelState>  {

    constructor(props: NewChannelProps) {
        super(props);
        this.state = {
            channelName: "",
            modalOpen: false
        }
    }

    createChannel = async (e: SyntheticEvent<HTMLElement>) => {
        e.preventDefault();
        if(this.state.channelName) {
            try {
                await createChannel(this.props.member, this.state.channelName);
                this.handleClose();
                this.props.onNewChannelCreated();
            } catch(e) {
                console.error(e);
            }

        }
    };

    handleOpen = () => this.setState({ modalOpen: true });
    handleClose = () => this.setState({ modalOpen: false });

    render() {
        return <Modal open={this.state.modalOpen} onClose={this.handleClose} trigger={<Button onClick={this.handleOpen}>Add Channel</Button>}>
            <Modal.Header>Set up your identity</Modal.Header>
            <Modal.Content>
                <h5>Name of the channel: </h5>
                <Input fluid onChange={(e, data) => {e.preventDefault(); this.setState({channelName: data.value})}}/>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    primary
                    icon={"add"}
                    labelPosition={'right'}
                    content={"Create channel"}
                    onClick={this.createChannel}
                />
            </Modal.Actions>
        </Modal>

    }
}