
export interface Member {
    privKey: string,
    pubKey: string,
}

export interface Message {
    author: string;
    message: string;
    timestamp: string;
}

export interface ChannelPreview {
    name: string,
    lastMessage: string,
    time: string
}