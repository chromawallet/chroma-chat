


export function getCompactDate(timestamp: string) {
    let date;
    if(!isNaN(timestamp as unknown as number)) {
        date = new Date(timestamp);
        date = `${date.getDay()}/${date.getMonth()}/${date.getFullYear()} - ${date.getHours()}:${date.getMinutes() < 10? '0' + date.getMinutes() : date.getMinutes()}`
    } else {
        date = 'na'
    }
    return date;
}