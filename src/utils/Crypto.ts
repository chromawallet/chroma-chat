const secp256k1 = require('secp256k1');
const randomBytes = require('crypto').randomBytes;

export function makeKeyPair() {
    let privKey;

    do {
        privKey = randomBytes(32);
    } while (!secp256k1.privateKeyVerify(privKey));

    return verifyKeyPair(privKey);
}

export function toBuffer(key: string): Buffer {
    return Buffer.from(key, "hex");
}

export function verifyKeyPair(privKey: Buffer) {
    const pubKey = secp256k1.publicKeyCreate(privKey);
    return {pubKey, privKey};
}