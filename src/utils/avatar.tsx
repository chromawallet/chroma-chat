import SpriteCollection from "@dicebear/avatars-male-sprites";
import Avatars from "@dicebear/avatars";

export function generateImg(seed: string){
    let avatars = new Avatars(SpriteCollection);
    let svg = avatars.create(seed);
    return svg;
}