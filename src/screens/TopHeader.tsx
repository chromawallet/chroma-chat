import * as React from "react";

import {Button, Icon, Image} from 'semantic-ui-react';
import {Member} from "../types";
import {generateImg} from "../utils/avatar";
import * as DOMPurify from "dompurify";
import {getBalance} from '../bc-logic/blockchain';
import {SyntheticEvent} from "react";

export interface TopHeaderProps {
    onOperationPerformed: Function;
    member: Member;
}

export interface TopHeaderState {
    image: any,
    balance: number | null,
}

export class TopHeader extends React.Component<TopHeaderProps, TopHeaderState> {

    constructor(props: TopHeaderProps) {
        super(props);

        this.state = {
            image: generateImg(props.member.pubKey.toLowerCase()),
            balance: null,
        }

    }

    async componentDidMount() {
        const balance = await getBalance(this.props.member);
        this.setState({balance});
    }

    logOut = (e: SyntheticEvent<HTMLElement>) => {
        e.preventDefault();
        window.localStorage.removeItem("member");
        this.props.onOperationPerformed();
        window.location.reload();
    };

    render() {
        return <header>
            <div style={{marginTop: 10, marginLeft: 10, float: 'left'}}>
                <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png'
                       size='mini' circular/>
            </div>
            <div>
                <h1>Channels App</h1>
            </div>
            <div style={{float: 'right'}} className="avatar">
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <div
                        dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(this.state.image)}}></div>
                    <Button
                        floated={'right'}
                        size={"mini"}
                        onClick={this.logOut}
                        icon>
                            <Icon name={"log out"}/>
                    </Button>
                </div>
                <h4>Balance: {this.state.balance} chromas</h4>
            </div>
        </header>
    }
}