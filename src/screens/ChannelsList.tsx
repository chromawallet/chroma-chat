import * as React from 'react';

import {Button, Header, Item} from 'semantic-ui-react';
import {getChannels, getLastMessage} from '../bc-logic/blockchain';
import {ChannelPreview, Member} from "../types";
import {NewChannel} from "../components/NewChannel";
import {SyntheticEvent} from "react";
import {InviteUser} from "../components/InviteUser";
import {getCompactDate} from "../utils/Date";

export interface ChannelsListProps {
    member: Member;
    setSelectedChannel: Function;
    refresh: boolean;
}

export interface ChannelsListState {
    channels: ChannelPreview[];
}

export class ChannelsList extends React.Component<ChannelsListProps, ChannelsListState> {
    constructor(props: ChannelsListProps) {
        super(props);

        this.state = {
            channels: [],
        }

    }

    async componentWillReceiveProps(newProps: ChannelsListProps) {
        if(newProps.refresh){
            const channels = await this.getChannels();
            this.setState({channels});
        }
    }

    async componentDidMount() {
        const channels = await this.getChannels();
        this.setState({channels});
    }

    onNewChannelCreated = async () => {
        const channels = await this.getChannels();
        this.setState({channels});
    }

    getChannels = async () => {
        const channelsPreview: ChannelPreview[] = [];
        const channels = await getChannels(this.props.member);
        for (const channel of channels) {
            const channelPreview = await this.getLastMessage(channel);
            channelsPreview.push(channelPreview);
        }
        return channelsPreview.sort((channel1: ChannelPreview, channel2: ChannelPreview) => {
            return (channel2.time as unknown as number) - (channel1.time as unknown as number);
        });
    };

    getLastMessage = async (channel: string): Promise<ChannelPreview> => {
        const lastMessage = await getLastMessage(channel);

        if (lastMessage) {
            let lastMessageText = lastMessage[0] as string;
            if( lastMessageText.length > 60) {
               lastMessageText = lastMessageText.substr(0, 60) + "..."
            }
            return {
                name: channel,
                lastMessage: lastMessageText,
                time: lastMessage[2]
            };
        } else {
            // on this channel no message had ever been sent
            return {
                name: channel,
                lastMessage: "Be the first one to post a message!",
                time: "na"
            };
        }
    };

    selectChannel = async (e: SyntheticEvent<HTMLDivElement>, key: number) => {
        e.preventDefault();
        this.props.setSelectedChannel(this.state.channels[key].name);
    };

    public render() {
        return <article className={"channels"}>
            <header>
                <Header as={'h1'}>Channels</Header>
            </header>
            <main className={"scrollableList"}>
                <Item>
                    {this.state.channels.map((channel: ChannelPreview, index: number) => {
                        // let date;
                        // if (!isNaN(parseInt(channel.time))){
                        //     date = new Date();
                        // } else {
                        //     date = channel.time
                        // }
                        const date = channel.time;
                        return (
                            <div key={index} className={"channel"} onClick={(e: SyntheticEvent<HTMLDivElement>) => this.selectChannel(e, index)}>
                                <Item.Content style={{textAlign: 'left'}}>
                                    <Item.Header as='h2'>{channel.name}</Item.Header>
                                    <Item.Meta>{channel.lastMessage}</Item.Meta>
                                    {/*<Item.Description>*/}
                                    {/*{channel.lastMessage}*/}
                                    {/*</Item.Description>*/}
                                    <Item.Extra style={{textAlign: 'right'}}>{getCompactDate(date)}</Item.Extra>
                                </Item.Content>
                            </div>
                        )
                    })}
                </Item>
            </main>
            <footer className="footer-list">
                <Button.Group widths='3'>
                    <NewChannel member={this.props.member} onNewChannelCreated={this.onNewChannelCreated}/>
                    <InviteUser member={this.props.member}/>
                    <Button>Support</Button>
                </Button.Group>
            </footer>
        </article>
    }
}