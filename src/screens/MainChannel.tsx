import * as React from 'react';

import {Divider} from 'semantic-ui-react';
import {getLastMessages, getMessagesSince} from '../bc-logic/blockchain';
import {NewMessage} from "../components/NewMessage";
import {Member, Message} from "../types";
import {AddUserToChannel} from "../components/AddUserToChannel";
import {getCompactDate} from "../utils/Date";

export interface MainChannelProps {
    member: Member;
    channel: string;
    onOperationPerformed: Function;
}

export interface MainChannelState {
    lastMessages: Message[];
}

export class MainChannel extends React.Component<MainChannelProps, MainChannelState> {

    constructor(props: MainChannelProps) {
        super(props);

        this.state = {
            lastMessages: []
        };
    }

    lowestTimestamp = Number.MAX_VALUE;
    interval: any = null;

    async componentWillReceiveProps(props: MainChannelProps) {
        if (props.channel != "") {
            this.getLastMessages(props.channel)
        }

        if(props.channel != this.props.channel) {
            this.setState({lastMessages: []})
        }

    }


    async getLastMessages(channel: string) {
        const lastMessagesRaw = await getLastMessages(channel);

        if (lastMessagesRaw != null && lastMessagesRaw.length > 0){
            const lastMessages = lastMessagesRaw.map((rawMessage: any, index: number) => {
                if(lastMessagesRaw.length -1 == index) this.lowestTimestamp = rawMessage[2] as unknown as number;
                return {
                    message: rawMessage[0],
                    author: rawMessage[1],
                    timestamp: rawMessage[2],
                }
            });

            this.setState({
                lastMessages,
            })
        }
    }

    async getMessagesSince(channel: string, since: number) {
        const messagesSinceRaw = await getMessagesSince(channel, since);

        if (!messagesSinceRaw) return [];

        const messageSince = messagesSinceRaw.map((rawMessage: any) => {
            return {
                message: rawMessage[0],
                author: rawMessage[1],
                timestamp: rawMessage[2],
            }
        });
        return messageSince;
    }

    onNewMessage = () => {
        this.props.onOperationPerformed();
    };

    handleScroll = async (e: any) => {
        // We want to load more messages when we almost reach the top - Say 150px
        const top = e.target.scrollTop <= 0;

        if (top && this.state.lastMessages && this.state.lastMessages.length > 0) {

            // Pull other messages
            // Get the oldest message timestamp
            this.lowestTimestamp = this.lowestTimestamp - 2000;
            const olderMessages = await this.getMessagesSince(this.props.channel, this.lowestTimestamp);
            const messages = this.state.lastMessages;


            for(let i = 0; i > olderMessages.length; i++){
                messages.push({
                    author: olderMessages[i].author,
                    message: olderMessages[i].message,
                    timestamp: olderMessages[i].timestamp as string
                });
            }

            this.setState({
                lastMessages: messages
            });
        }
    };


    render() {
        const channel = this.props.channel;
        const member = this.props.member;
        const lastMessages = this.state.lastMessages;
        return this.props.channel? <article className={"channels"}>
                <header>
                    <div style={{display: 'grid'}}>
                        <div style={{display: "inline-table"}}>
                            <h1 style={{textAlign: 'left'}}>{channel}</h1>
                            <AddUserToChannel member={this.props.member} channel={this.props.channel}/>
                        </div>
                        <Divider/>
                    </div>
                </header>
                <main
                    className={"scrollableList chatWindow"}
                    // onScroll={this.handleScroll}
                >
                    {lastMessages.length != 0 && lastMessages.map((message: Message, index: number) => {
                        return message.author == this.props.member.pubKey ?
                            <div className={"chat"} key={index} style={{marginLeft: '4em'}}>
                                <p style={{textAlign: 'left'}}>{message.message}</p>
                                <p style={{textAlign: "right"}}>{getCompactDate(message.timestamp)}</p>
                            </div>
                            :
                            <div className={"chat"} key={index}
                                 style={{marginRight: '4em', backgroundColor: 'lightgray'}}>
                                <p style={{textAlign: 'left'}}>{message.message}</p>
                                <p style={{textAlign: "right"}}>{getCompactDate(message.timestamp)}</p>
                            </div>
                    })}
                </main>
                <footer className="footer-list">
                    <NewMessage memeber={member} channel={channel} onNewMessage={this.onNewMessage}/>
                </footer>
            </article>

            :

            <div className={'vertical-align-message'}>
                <h1>Create or select a channel.</h1>
            </div>
    }
}