import * as React from 'react';
import {Grid} from 'semantic-ui-react'
import './App.css';

import {ChannelsList} from "./screens/ChannelsList";
import {Member} from "./types";
import {Login} from "./components/Login";
import {TopHeader} from "./screens/TopHeader";
import {MainChannel} from "./screens/MainChannel";
import {init} from './bc-logic/blockchain';

export interface AppProps {
}

export interface AppState {
    member: Member | null;
    selectedChannel: string; // channel name
    refreshChannelList: boolean;
}

class App extends React.Component<AppProps, AppState> {

    constructor(props: any) {
        super(props);

        const localMemeber = window.localStorage.getItem('member');
        if (localMemeber) {
            console.log("Key Pair", localMemeber)
            const member: Member = JSON.parse(localMemeber!) as Member;
            this.state = {
                member,
                selectedChannel: "",
                refreshChannelList: false,
            }

        } else {
            this.state = {
                member: null,
                selectedChannel: "",
                refreshChannelList: false
            }
        }

        try {
            init();
            console.log("NOTHING HAPPENED ( ͡° ͜ʖ ͡°)")
        }catch(e) {
            console.log("NOTHING HAPPENED ( ͡° ͜ʖ ͡°)")
        }
        this.login = this.login.bind(this);

    }

    componentDidMount = () => {
        setInterval(() => {
            this.setState({
                refreshChannelList: true
            });
        }, 3000)
    }


    async login(member: Member) {
        this.setState({member});
        window.localStorage.setItem('member', `{ "pubKey": "${member.pubKey}", "privKey": "${member.privKey}" }`);
    }

    setSelectedChannel = (channel: string) => {
        this.setState({
            selectedChannel: channel,
        });
    };

    onOperationPerformed = () => {
        this.setState({refreshChannelList: true});
        this.forceUpdate();
    };

    public render() {
        return (
            this.state.member ?
                <div className="App">
                    <div className={'my-header'}>
                        <TopHeader member={this.state.member} onOperationPerformed={this.onOperationPerformed} />
                    </div>
                    <Grid columns={2} divided className={"two-columns-layout"}>
                        <Grid.Row style={{ height: '90vh', padding: 0}}>
                            <Grid.Column width={6} style={{height: '100%', paddingRight:0}}>
                                <ChannelsList member={this.state.member} setSelectedChannel={this.setSelectedChannel} refresh={this.state.refreshChannelList}/>
                            </Grid.Column>
                            <Grid.Column width={10} style={{padding: 0}}>
                                <MainChannel member={this.state.member} channel={this.state.selectedChannel} onOperationPerformed={this.onOperationPerformed}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
                :
                <Login login={this.login}/>
        );
    }
}

export default App;
