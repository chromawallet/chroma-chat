import * as pcl from 'postchain-client';
import * as crypto from 'crypto'
import * as secp256k1 from 'secp256k1'


const rest = pcl.restClient.createRestClient("http://localhost:7740/", '78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3', 5)
const gtx = pcl.gtxClient.createClient(
    rest,
    Buffer.from(
        '78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3',
        'hex'
    ),
    []
);

const adminPUB = Buffer.from(
    '031b84c5567b126440995d3ed5aaba0565d71e1834604819ff9c17f5e9d5dd078f',
    'hex'
);
const adminPRIV = Buffer.from(
    '0101010101010101010101010101010101010101010101010101010101010101',
    'hex'
);

function toBuffer(key) {
    return Buffer.from(key, "hex");
}

function wrapInAPromise(request) {
    return new Promise((resolve, reject) => gtx.query(request, (err, res) => {
            if(err) {
                console.log('Error', err);
                reject(err);
            }
            resolve(res);
        }
    ));
}

export function make_tuid() {
    return crypto.randomBytes(16).toString('hex')
}

export function makeKeyPair() {
    let privKey
    do {
        privKey = crypto.randomBytes(32)
    } while (!secp256k1.privateKeyVerify(privKey))
    const pubKey = secp256k1.publicKeyCreate(privKey)
    return {pubKey, privKey}
}

export function init(){
    const rq = gtx.newRequest([adminPUB]);
    rq.addCall('init', adminPUB);
    rq.sign(adminPRIV, adminPUB);
    return rq.postAndWaitConfirmation();
}

export function createChannel(admin, channelName) {
    const pubKey = Buffer.from(admin.pubKey, 'hex');
    const privKey = Buffer.from(admin.privKey, 'hex');
    const rq = gtx.newRequest([pubKey]);
    rq.addCall("create_channel", pubKey, channelName);
    rq.sign(privKey, pubKey);
    return rq.postAndWaitConfirmation();
}

export function postMessage(user, channelName, message){
    const pubKey = Buffer.from(user.pubKey, 'hex');
    const privKey = Buffer.from(user.privKey, 'hex');
    const rq = gtx.newRequest([pubKey]);
    rq.addCall("post_message", channelName, pubKey,  message, crypto.randomBytes(32));
    // rq.addCall("nop", crypto.randomBytes(20));
    rq.sign(privKey, pubKey);
    return rq.postAndWaitConfirmation();
}


export function inviteUser(existingUser, newUserPubKey, startAmount) {
    const pubKey = toBuffer(existingUser.pubKey);
    const privKey = toBuffer(existingUser.privKey);
    const rq = gtx.newRequest([pubKey]);
    rq.addCall("register_user", pubKey, toBuffer(newUserPubKey), parseInt(startAmount));
    rq.sign(privKey, pubKey);
    return rq.postAndWaitConfirmation();
}

export function inviteUserToChat(existingUser, channel, newUserPubKey) {
    const pubKey = toBuffer(existingUser.pubKey);
    const privKey = toBuffer(existingUser.privKey);
    const rq = gtx.newRequest([pubKey]);
    rq.addCall("add_channel_member", pubKey, channel, toBuffer(newUserPubKey));
    rq.sign(privKey, pubKey);
    return rq.postAndWaitConfirmation();
}

export function getBalance(user) {
    return new Promise((resolve, reject) => gtx.query( {
        type: "get_balance",
        q_user_pubkey: user.pubKey
    }, (err, res) => {
            if(err) reject(err);
            resolve(res);
        }
    ));
}

export function getChannels(user) {
    return new Promise((resolve, reject) => gtx.query( {
            type: "get_channels",
            q_user_pubkey: user.pubKey
        }, (err, res) => {
            if(err) reject(err);
            resolve(res);
        }
    ));
}

export function getLastMessages(channel) {
    return wrapInAPromise({type: "get_last_messages", q_channel_name: channel});
}

export function getMessagesSince(channel, since) {
    return wrapInAPromise({type: "get_messages_since", q_channel_name: channel, q_since_timestamp: since});
}

export function getLastMessage(channelName) {
    return new Promise((resolve, reject) => gtx.query({
        type: "get_last_message",
        q_channel_name: channelName
    }, (err, res) => {
        if(err) reject(err);
        resolve(res);
    }));
}